# Contributing Guidelines

Contributions are welcome via Gitlab Pull Requests. This document outlines the process to help get your contribution accepted.

Any type of contribution is welcome: new code snippets, secure code for a language which is not initially present, bug fixes, documentation improvements, etc.

## How to Contribute

Fork this repository, develop, and test your changes.
Submit a pull request.

## Requirements

### When submitting a PR make sure that:

* It must pass CI jobs for linting and test the changes (if any).
* The title of the PR is clear enough.
* If necessary, add information to the repository's README.md.
* PR Approval and Release Process
* Changes are manually reviewed by Payatu team members.
* The PR is merged by the reviewer(s) in the Gitlab's master branch.
* Then our CI/CD system is going to push the changes to the website including the recently merged changes.

## To add your contributor profile

Create a new directory inside `content > contributors`, with your name and inside that a file named `_index.md`, where all the details will be there.

Use the template (/author-name/) , in the `contributors` directory.
