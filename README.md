# Secure Code Wiki by Payatu 

## Process to upload content
Download the [template](./template.md), and make necessary changes and upload it in the `content/docs/lang/` directory.

Change the front matter, example below
```md
---
title: "Golang"
description: "Secure Coding Practices for Golang."
lead: "Recommendations for OWASP Top 10"
draft: false
images: []
menu:
  docs:
    parent: "docs"
toc: true
# author: Kumar Ashwin
---
```
The upload directory hierarchy is as follow,
```
.
|- content
    |- docs
        |- lang
            |- your-file.md

```

## To add your contributor profile
Create a new directory inside `content > contributors`, with your name and inside that a file named `_index.md`, where all the details will be there.

Use the template (/author-name/) , in the `contributors` directory.

## Note
In case of any query/issues contact `kumar@payatu.io`/`aman@payatu.com` or `ashwin`/`aman` in the rc.